# flacon

An Audio File Encoder. Extracts audio tracks from an audio CD image to separate tracks.

https://flacon.github.io/

https://github.com/flacon/flacon

<br>

How to clone this repository:

```
git clone https://gitlab.com/azul4/content/multimedia/flacon.git
```

